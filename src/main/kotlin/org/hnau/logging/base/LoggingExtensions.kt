package org.hnau.logging.base

import org.slf4j.Logger
import org.slf4j.LoggerFactory


inline fun <reified T> logger(): Logger =
        LoggerFactory.getLogger(T::class.java)

fun logger(name: String): Logger =
        LoggerFactory.getLogger(name)