package org.hnau.logging.base

import org.slf4j.Logger
import org.slf4j.event.Level

fun Logger.log(
        level: Level,
        message: Any,
        throwable: Throwable? = null
) = message.toString().let { msg ->
    when (level) {
        Level.ERROR -> error(msg, throwable)
        Level.WARN -> warn(msg, throwable)
        Level.INFO -> info(msg, throwable)
        Level.DEBUG -> debug(msg, throwable)
        Level.TRACE -> trace(msg, throwable)
    }
}

fun Logger.trace(message: Any, throwable: Throwable? = null) =
        log(Level.TRACE, message, throwable)

fun Logger.debug(message: Any, throwable: Throwable? = null) =
        log(Level.DEBUG, message, throwable)

fun Logger.info(message: Any, throwable: Throwable? = null) =
        log(Level.INFO, message, throwable)

fun Logger.warn(message: Any, throwable: Throwable? = null) =
        log(Level.WARN, message, throwable)

fun Logger.error(message: Any, throwable: Throwable? = null) =
        log(Level.ERROR, message, throwable)