package org.hnau.logging.base

import org.slf4j.Logger
import org.slf4j.Marker

abstract class PrepareMessageLogger(
        private val parent: Logger
) : Logger {

    companion object {}

    protected abstract fun prepareMessage(text: String): String

    private fun prepare(text: String?) =
            text?.let(::prepareMessage)

    override fun warn(text: String?) =
            parent.warn(prepare(text))

    override fun warn(text: String?, arg: Any?) =
            parent.warn(prepare(text), arg)

    override fun warn(text: String?, vararg args: Any?) =
            parent.warn(prepare(text), *args)

    override fun warn(text: String?, arg1: Any?, arg2: Any?) =
            parent.warn(prepare(text), arg1, arg2)

    override fun warn(text: String?, throwable: Throwable?) =
            parent.warn(prepare(text), throwable)

    override fun warn(marker: Marker?, text: String?) =
            parent.warn(marker, prepare(text))

    override fun warn(marker: Marker?, text: String?, arg: Any?) =
            parent.warn(marker, prepare(text), arg)

    override fun warn(marker: Marker?, text: String?, arg1: Any?, arg2: Any?) =
            parent.warn(marker, prepare(text), arg1, arg2)

    override fun warn(marker: Marker?, text: String?, vararg args: Any?) =
            parent.warn(marker, prepare(text), *args)

    override fun warn(marker: Marker?, text: String?, throwable: Throwable?) =
            parent.warn(marker, prepare(text), throwable)

    override fun info(text: String?) =
            parent.info(prepare(text))

    override fun info(text: String?, arg: Any?) =
            parent.info(prepare(text), arg)

    override fun info(text: String?, vararg args: Any?) =
            parent.info(prepare(text), *args)

    override fun info(text: String?, arg1: Any?, arg2: Any?) =
            parent.info(prepare(text), arg1, arg2)

    override fun info(text: String?, throwable: Throwable?) =
            parent.info(prepare(text), throwable)

    override fun info(marker: Marker?, text: String?) =
            parent.info(marker, prepare(text))

    override fun info(marker: Marker?, text: String?, arg: Any?) =
            parent.info(marker, prepare(text), arg)

    override fun info(marker: Marker?, text: String?, arg1: Any?, arg2: Any?) =
            parent.info(marker, prepare(text), arg1, arg2)

    override fun info(marker: Marker?, text: String?, vararg args: Any?) =
            parent.info(marker, prepare(text), *args)

    override fun info(marker: Marker?, text: String?, throwable: Throwable?) =
            parent.info(marker, prepare(text), throwable)

    override fun debug(text: String?) =
            parent.debug(prepare(text))

    override fun debug(text: String?, arg: Any?) =
            parent.debug(prepare(text), arg)

    override fun debug(text: String?, vararg args: Any?) =
            parent.debug(prepare(text), *args)

    override fun debug(text: String?, arg1: Any?, arg2: Any?) =
            parent.debug(prepare(text), arg1, arg2)

    override fun debug(text: String?, throwable: Throwable?) =
            parent.debug(prepare(text), throwable)

    override fun debug(marker: Marker?, text: String?) =
            parent.debug(marker, prepare(text))

    override fun debug(marker: Marker?, text: String?, arg: Any?) =
            parent.debug(marker, prepare(text), arg)

    override fun debug(marker: Marker?, text: String?, arg1: Any?, arg2: Any?) =
            parent.debug(marker, prepare(text), arg1, arg2)

    override fun debug(marker: Marker?, text: String?, vararg args: Any?) =
            parent.debug(marker, prepare(text), *args)

    override fun debug(marker: Marker?, text: String?, throwable: Throwable?) =
            parent.debug(marker, prepare(text), throwable)

    override fun error(text: String?) =
            parent.error(prepare(text))

    override fun error(text: String?, arg: Any?) =
            parent.error(prepare(text), arg)

    override fun error(text: String?, vararg args: Any?) =
            parent.error(prepare(text), *args)

    override fun error(text: String?, arg1: Any?, arg2: Any?) =
            parent.error(prepare(text), arg1, arg2)

    override fun error(text: String?, throwable: Throwable?) =
            parent.error(prepare(text), throwable)

    override fun error(marker: Marker?, text: String?) =
            parent.error(marker, prepare(text))

    override fun error(marker: Marker?, text: String?, arg: Any?) =
            parent.error(marker, prepare(text), arg)

    override fun error(marker: Marker?, text: String?, arg1: Any?, arg2: Any?) =
            parent.error(marker, prepare(text), arg1, arg2)

    override fun error(marker: Marker?, text: String?, vararg args: Any?) =
            parent.error(marker, prepare(text), *args)

    override fun error(marker: Marker?, text: String?, throwable: Throwable?) =
            parent.error(marker, prepare(text), throwable)

    override fun trace(text: String?) =
            parent.trace(prepare(text))

    override fun trace(text: String?, arg: Any?) =
            parent.trace(prepare(text), arg)

    override fun trace(text: String?, vararg args: Any?) =
            parent.trace(prepare(text), *args)

    override fun trace(text: String?, arg1: Any?, arg2: Any?) =
            parent.trace(prepare(text), arg1, arg2)

    override fun trace(text: String?, throwable: Throwable?) =
            parent.trace(prepare(text), throwable)

    override fun trace(marker: Marker?, text: String?) =
            parent.trace(marker, prepare(text))

    override fun trace(marker: Marker?, text: String?, arg: Any?) =
            parent.trace(marker, prepare(text), arg)

    override fun trace(marker: Marker?, text: String?, arg1: Any?, arg2: Any?) =
            parent.trace(marker, prepare(text), arg1, arg2)

    override fun trace(marker: Marker?, text: String?, vararg args: Any?) =
            parent.trace(marker, prepare(text), *args)

    override fun trace(marker: Marker?, text: String?, throwable: Throwable?) =
            parent.trace(marker, prepare(text), throwable)

    override fun getName() = parent.name

    override fun isErrorEnabled() = parent.isErrorEnabled

    override fun isErrorEnabled(marker: Marker?) = parent.isErrorEnabled(marker)

    override fun isWarnEnabled() = parent.isWarnEnabled

    override fun isWarnEnabled(marker: Marker?) = parent.isWarnEnabled(marker)

    override fun isDebugEnabled() = parent.isDebugEnabled

    override fun isDebugEnabled(marker: Marker?) = parent.isDebugEnabled(marker)

    override fun isInfoEnabled() = parent.isInfoEnabled

    override fun isInfoEnabled(marker: Marker?) = parent.isInfoEnabled(marker)

    override fun isTraceEnabled() = parent.isTraceEnabled

    override fun isTraceEnabled(marker: Marker?) = parent.isTraceEnabled(marker)

}