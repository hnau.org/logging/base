package org.hnau.logging.base.tracer.utils

import org.hnau.base.extensions.checkNullable
import org.hnau.base.extensions.number.takeIfNotNegative
import org.hnau.base.extensions.stackHead
import org.hnau.logging.base.tracer.Tracer
import org.hnau.logging.base.tracer.map
import org.hnau.logging.base.tracer.message.Message


data class Location(
        val location: String
) {

    companion object {

        private const val partsSeparator = "."

    }

    fun next(
            nextPart: String
    ) = Location(
            location = location + partsSeparator + nextPart
    )

    override fun toString() = location

}

inline fun <reified T> location() = Location(T::class.java.name)

fun Location.Companion.create(
        stackTraceElement: StackTraceElement
) = listOf(
        stackTraceElement.className.let { className ->
            className
                    .indexOf('$')
                    .takeIfNotNegative()
                    .checkNullable(
                            ifNotNull = { separatorIndex -> className.substring(0, separatorIndex) },
                            ifNull = { className }
                    )
        },
        stackTraceElement.methodName
)
        .joinToString(".")
        .let(::Location)

@Suppress("NOTHING_TO_INLINE")
inline fun Location.Companion.here() =
        Location.create(stackHead())

operator fun Location.plus(nextPart: String) = next(nextPart)

data class LocatedMessage(
        val location: Location,
        val content: Message
)

fun Tracer<LocatedMessage>.locate(
        location: Location
) = map<LocatedMessage, Message> { content ->
    LocatedMessage(
            location = location,
            content = content
    )
}

@Suppress("NOTHING_TO_INLINE")
inline fun Tracer<LocatedMessage>.locateHere() =
        locate(Location.here())