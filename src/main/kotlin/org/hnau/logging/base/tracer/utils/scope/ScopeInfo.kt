package org.hnau.logging.base.tracer.utils.scope

import org.hnau.base.data.time.Time
import org.hnau.base.data.time.now
import org.hnau.base.extensions.uuid.toCompactString
import java.util.*


data class ScopeInfo(
        val id: String = UUID.randomUUID().toCompactString(),
        val started: Time = Time.now()
)