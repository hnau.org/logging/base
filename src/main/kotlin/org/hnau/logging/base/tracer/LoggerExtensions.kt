package org.hnau.logging.base.tracer

import org.hnau.base.data.associator.Associator
import org.hnau.base.data.associator.byHashMap
import org.hnau.base.data.associator.toAutoAssociator
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.now
import org.hnau.base.data.time.toLevelsString
import org.hnau.logging.base.log
import org.hnau.logging.base.tracer.message.Level
import org.hnau.logging.base.tracer.message.Message
import org.hnau.logging.base.tracer.message.Stage
import org.hnau.logging.base.tracer.utils.LocatedMessage
import org.hnau.logging.base.tracer.utils.scope.ScopeMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory


inline fun <M> Tracers.logging(
        crossinline extractLoggerName: (M) -> String,
        crossinline log: Logger.(M) -> Unit
): Tracer<M> {

    val loggerGetter = Associator
            .byHashMap<String, Logger>()
            .toAutoAssociator(LoggerFactory::getLogger)

    return { message ->
        val loggerName = extractLoggerName(message)
        val logger = loggerGetter(loggerName)
        logger.log(message)
    }
}

val Message.logLevel: org.slf4j.event.Level
    get() = when (stage) {
        Stage.system -> when (level) {
            Level.expected -> org.slf4j.event.Level.DEBUG
            Level.unusual -> org.slf4j.event.Level.ERROR
        }
        Stage.logic -> when (level) {
            Level.expected -> org.slf4j.event.Level.INFO
            Level.unusual -> org.slf4j.event.Level.WARN
        }
    }

fun Tracers.logging(): Tracer<LocatedMessage> = logging(
        extractLoggerName = { it.location.location },
        log = { message ->
            log(
                    level = message.content.logLevel,
                    message = message.content.text
            )
        }
)

fun Tracers.loggingWithScope() = logging()
        .map<LocatedMessage, ScopeMessage> { (scopeInfo, locatedMessage) ->
            val afterStarted = (Time.now() - scopeInfo.started).toLevelsString()
            locatedMessage.copy(
                    content = locatedMessage.content.copy(
                            text = "[${scopeInfo.id}/$afterStarted] ${locatedMessage.content.text}"
                    )
            )
        }