package org.hnau.logging.base.tracer.utils.scope

import org.hnau.logging.base.tracer.Tracer
import org.hnau.logging.base.tracer.map
import org.hnau.logging.base.tracer.utils.LocatedMessage


data class ScopeMessage(
        val scopeInfo: ScopeInfo,
        val content: LocatedMessage
)

fun Tracer<ScopeMessage>.withScope(
        scopeInfo: ScopeInfo = ScopeInfo()
) = map<ScopeMessage, LocatedMessage> { content ->
    ScopeMessage(
            scopeInfo = scopeInfo,
            content = content
    )
}
