package org.hnau.logging.base.tracer.message


enum class Stage {

    system,
    logic

}