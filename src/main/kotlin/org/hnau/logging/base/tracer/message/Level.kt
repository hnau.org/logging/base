package org.hnau.logging.base.tracer.message


enum class Level {

    expected,
    unusual

}