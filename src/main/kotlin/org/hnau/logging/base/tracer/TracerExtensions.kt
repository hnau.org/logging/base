package org.hnau.logging.base.tracer


inline fun <I, O> Tracer<I>.map(
        crossinline convert: (O) -> I
): Tracer<O> = { message ->
    trace(convert(message))
}

fun <M> Tracer<M>.trace(
        message: M
) = invoke(
        message
)