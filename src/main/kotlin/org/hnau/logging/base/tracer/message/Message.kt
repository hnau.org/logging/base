package org.hnau.logging.base.tracer.message

import org.hnau.logging.base.tracer.Tracer
import org.hnau.logging.base.tracer.map
import org.hnau.logging.base.tracer.trace


data class Message(
        val stage: Stage,
        val level: Level,
        val text: String
)

fun Tracer<Message>.trace(
        stage: Stage,
        level: Level,
        text: String
) = trace(
        message = Message(
                stage = stage,
                level = level,
                text = text
        )
)

data class StageMessage(
        val level: Level,
        val text: String
)

fun Tracer<Message>.stage(
        stage: Stage
) = map<Message, StageMessage> { stageMessage ->
    Message(
            stage = stage,
            level = stageMessage.level,
            text = stageMessage.text
    )
}

val Tracer<Message>.system get() = stage(Stage.system)
val Tracer<Message>.logic get() = stage(Stage.logic)


fun Tracer<StageMessage>.trace(
        level: Level,
        text: String
) = trace(
        StageMessage(
                level = level,
                text = text
        )
)

fun Tracer<StageMessage>.expected(text: String) = trace(Level.expected, text)
fun Tracer<StageMessage>.unusual(text: String) = trace(Level.unusual, text)

