package org.hnau.logging.base.tracer


typealias Tracer<M> = (M) -> Unit