package org.hnau.logger.base.tracer.utils

import org.hnau.logging.base.tracer.Tracer
import org.hnau.logging.base.tracer.trace
import org.hnau.logging.base.tracer.utils.LocatedMessage
import org.hnau.logging.base.tracer.utils.Location
import org.hnau.logging.base.tracer.utils.locateHere
import org.junit.Test
import org.assertj.core.api.Assertions.assertThat
import org.hnau.logging.base.tracer.message.expected
import org.hnau.logging.base.tracer.message.logic


class LocationTests {

    @Test
    fun test() {

        lateinit var receiverLocation: Location

        val tracer: Tracer<LocatedMessage> = { message ->
            receiverLocation = message.location
        }

        tracer.locateHere().logic.expected("Test")

        assertThat(receiverLocation)
                .isEqualTo(Location("org.hnau.logger.base.tracer.utils.LocationTests.test"))


    }

}